echo "Wait that SQL Server came up"
sleep 5
until lsof -i -P -n | grep v4 | grep -q ":1433 (LISTEN)";
do
  sleep 1
done
sleep 1

if [ ! -f /var/opt/mssql/data/Test.mdf ]; then
  echo "Start create DB Test"
  /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P S0lid@Test -d master -i create_database.sql
  echo "DB Test created"
else
  echo "Database Test always exist"
fi
