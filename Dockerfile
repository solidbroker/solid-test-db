FROM mcr.microsoft.com/mssql/server:2017-latest

ENV SA_PASSWORD S0lid@Test
ENV ACCEPT_EULA Y
ENV MSSQL_PID Express

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY ./*.sh /usr/src/app
COPY ./*.sql /usr/src/app

RUN sed -i 's/\r//g' /usr/src/app/initialization.sh
RUN chmod +x /usr/src/app/initialization.sh

CMD /bin/bash ./entrypoint.sh
